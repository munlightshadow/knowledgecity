-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Авг 30 2021 г., 00:20
-- Версия сервера: 5.7.35-0ubuntu0.18.04.1
-- Версия PHP: 7.2.24-0ubuntu0.18.04.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `knowledgecity`
--

-- --------------------------------------------------------

--
-- Структура таблицы `api_users`
--

CREATE TABLE `api_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `api_users`
--

INSERT INTO `api_users` (`id`, `username`, `password`, `token`, `created_at`, `updated_at`) VALUES
(1, 'admin', '$2y$10$WDzJm18aOCBPajyAKObLyefGjrLFchdHZ934kqTcSOVQcCex2IDWC', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxLCJ0aW1lc3RhbXAiOjE2MzAyNzE0NjB9._xc7s-x8-mKq-SDoTJ59fPerFQE68EoewOkx38RX8zw', '2021-08-29 18:27:49', '2021-08-29 21:11:00'),
(2, 'user', '$2y$10$E1fqMaI5h0z4gGCC1GjlMudTYQFPC1.nqJ2Z4yXPA5vGPb6sfZyxm', NULL, '2021-08-29 18:27:50', '2021-08-29 18:27:50');

-- --------------------------------------------------------

--
-- Структура таблицы `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `students`
--

INSERT INTO `students` (`id`, `username`, `name`, `surname`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'admin_st_username_1', 'admin_st_name_1', 'admin_st_surname_1', 1, '2021-08-29 18:27:49', '2021-08-29 18:27:49'),
(2, 'admin_st_username_2', 'admin_st_name_2', 'admin_st_surname_2', 1, '2021-08-29 18:27:49', '2021-08-29 18:27:49'),
(3, 'admin_st_username_3', 'admin_st_name_3', 'admin_st_surname_3', 1, '2021-08-29 18:27:49', '2021-08-29 18:27:49'),
(4, 'admin_st_username_4', 'admin_st_name_4', 'admin_st_surname_4', 1, '2021-08-29 18:27:49', '2021-08-29 18:27:49'),
(5, 'admin_st_username_5', 'admin_st_name_5', 'admin_st_surname_5', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(6, 'admin_st_username_6', 'admin_st_name_6', 'admin_st_surname_6', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(7, 'admin_st_username_7', 'admin_st_name_7', 'admin_st_surname_7', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(8, 'admin_st_username_8', 'admin_st_name_8', 'admin_st_surname_8', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(9, 'admin_st_username_9', 'admin_st_name_9', 'admin_st_surname_9', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(10, 'admin_st_username_10', 'admin_st_name_10', 'admin_st_surname_10', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(11, 'admin_st_username_11', 'admin_st_name_11', 'admin_st_surname_11', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(12, 'admin_st_username_12', 'admin_st_name_12', 'admin_st_surname_12', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(13, 'admin_st_username_13', 'admin_st_name_13', 'admin_st_surname_13', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(14, 'admin_st_username_14', 'admin_st_name_14', 'admin_st_surname_14', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(15, 'admin_st_username_15', 'admin_st_name_15', 'admin_st_surname_15', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(16, 'admin_st_username_16', 'admin_st_name_16', 'admin_st_surname_16', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(17, 'admin_st_username_17', 'admin_st_name_17', 'admin_st_surname_17', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(18, 'admin_st_username_18', 'admin_st_name_18', 'admin_st_surname_18', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(19, 'admin_st_username_19', 'admin_st_name_19', 'admin_st_surname_19', 1, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(20, 'user_st_username_1', 'user_st_name_1', 'user_st_surname_1', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(21, 'user_st_username_2', 'user_st_name_2', 'user_st_surname_2', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(22, 'user_st_username_3', 'user_st_name_3', 'user_st_surname_3', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(23, 'user_st_username_4', 'user_st_name_4', 'user_st_surname_4', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(24, 'user_st_username_5', 'user_st_name_5', 'user_st_surname_5', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(25, 'user_st_username_6', 'user_st_name_6', 'user_st_surname_6', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(26, 'user_st_username_7', 'user_st_name_7', 'user_st_surname_7', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(27, 'user_st_username_8', 'user_st_name_8', 'user_st_surname_8', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(28, 'user_st_username_9', 'user_st_name_9', 'user_st_surname_9', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(29, 'user_st_username_10', 'user_st_name_10', 'user_st_surname_10', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(30, 'user_st_username_11', 'user_st_name_11', 'user_st_surname_11', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(31, 'user_st_username_12', 'user_st_name_12', 'user_st_surname_12', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(32, 'user_st_username_13', 'user_st_name_13', 'user_st_surname_13', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(33, 'user_st_username_14', 'user_st_name_14', 'user_st_surname_14', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(34, 'user_st_username_15', 'user_st_name_15', 'user_st_surname_15', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(35, 'user_st_username_16', 'user_st_name_16', 'user_st_surname_16', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(36, 'user_st_username_17', 'user_st_name_17', 'user_st_surname_17', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(37, 'user_st_username_18', 'user_st_name_18', 'user_st_surname_18', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50'),
(38, 'user_st_username_19', 'user_st_name_19', 'user_st_surname_19', 2, '2021-08-29 18:27:50', '2021-08-29 18:27:50');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `api_users`
--
ALTER TABLE `api_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `api_users_username_unique` (`username`);

--
-- Индексы таблицы `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`),
  ADD KEY `students_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `api_users`
--
ALTER TABLE `api_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `students`
--
ALTER TABLE `students`
  ADD CONSTRAINT `students_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `api_users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

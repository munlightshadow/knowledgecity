<?php

//add display error
error_reporting(E_ALL);
ini_set('display_errors', 'on');

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//add autoloader
require __DIR__ .'/vendor/autoload.php';
require __DIR__ . "/bootstrap.php";
require 'routes/route.php';

//add .env variable
$dotenv = Dotenv\Dotenv::createUnsafeImmutable(__DIR__);
$dotenv->load();

// php database/migrations.php
// php database/seeds.php
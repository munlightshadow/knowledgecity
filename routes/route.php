<?php

use Steampixel\Route;

use App\Controllers\UserController;
use App\Controllers\AuthController;

// example
// Route::add('/user/([0-9]*)/edit', function($id) {
//   echo 'Edit user with id '.$id.'<br>';
// }, 'get');

// Auth routs
Route::add('/auth', function() {return AuthController::login();}, 'post');
Route::add('/auth', function() {return AuthController::logout();}, 'delete');

// User routs
Route::add('/users', function() {return UserController::students();}, 'get');

// Run the router
Route::run('/');
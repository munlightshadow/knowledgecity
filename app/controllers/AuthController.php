<?php

namespace App\Controllers;

use App\Models\User;
use App\Helpers\JwtHelper;

/**
 * Student controller
 * @author Alexander Kalksov <munlightshadow@gmail.com>
 */
class AuthController
{
    static public function login()
    {
        $login = (isset($_POST['login']) ? $_POST['login'] : '');
        $password = (isset($_POST['password']) ? $_POST['password'] : '');

        $user = User::where('username', '=', $login)->first();

        if (!$user) {
            header("HTTP/1.1 401 Unauthorized");            
            return json_encode(['message' => 'login or password incorrect']);
        }

        if (password_verify($password, $user->password)) {
            $user->token = JwtHelper::generateToken($user->id);
            $user->save();

            return json_encode(['token' => $user->token]);
        } else {
            header("HTTP/1.1 401 Unauthorized");            
            return json_encode(['message' => 'login or password incorrect']);
        }
    }

    static public function logout()
    {
        $authUser = JwtHelper::checkToken();

        $authUser->token = Null;
        $authUser->save();

        return json_encode(['message' => 'Logout success']);
    }    
}
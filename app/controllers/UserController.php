<?php

namespace App\Controllers;

use App\Models\Student;
use App\Models\User;
use App\Helpers\JwtHelper;

/**
 * Student controller
 * @author Alexander Kalksov <munlightshadow@gmail.com>
 */
class UserController
{
    static public function students()
    {
        $authUser = JwtHelper::checkToken();

        $sort = (isset($_GET['sort']) ? $_GET['sort'] : 'id');
        $order = (isset($_GET['order']) ? $_GET['order'] : 'asc');
        $countOnPage = (isset($_GET['countOnPage']) ? $_GET['countOnPage'] : 5);
        $page = (isset($_GET['page']) ? $_GET['page'] : 1);

        $students = $authUser->students()->orderBy($sort, $order)->paginate($countOnPage, ['*'], 'page', $page)->toJson(); 

        return $students;
    }
}
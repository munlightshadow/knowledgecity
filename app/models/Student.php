<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Student extends Eloquent
{
    public function user()
    {
        return $this->hasOne('App\Models\User');
    }
}
<?php

namespace App\Models;

require __DIR__ . "/../../bootstrap.php";

use Illuminate\Database\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $table = 'api_users';

    protected $fillable = [
        'username'
    ];

    protected $hidden = [
        'password', 'token'
    ];

    public function students()
    {
        return $this->hasMany('App\Models\Student');
    }
}

<?php

require "bootstrap.php";

use App\Models\Student;
use App\Models\User;

$user = new User();
$user->username = 'admin';
$user->password = password_hash('qweqweqwe', PASSWORD_BCRYPT);
$user->save();

echo '------------- admin created -------------' . PHP_EOL;

for ($i=1; $i < 20; $i++) { 
   $student = new Student();
   $student->username = 'admin_st_username_' . $i;
   $student->name = 'admin_st_name_' . $i;
   $student->surname = 'admin_st_surname_' . $i;
   $student->user_id = $user->id;
   $student->save();

   echo 'students admin_st_username_' . $i . '  for admin created' . PHP_EOL;
}


$user = new User();
$user->username = 'user';
$user->password = password_hash('qweqweqwe', PASSWORD_BCRYPT);
$user->save();

echo '------------- user created -------------' . PHP_EOL;

for ($i=1; $i < 20; $i++) { 
   $student = new Student();
   $student->username = 'user_st_username_' . $i;
   $student->name = 'user_st_name_' . $i;
   $student->surname = 'user_st_surname_' . $i;
   $student->user_id = $user->id;
   $student->save();

   echo 'students admin_st_username_' . $i . '  for user created' . PHP_EOL;
}
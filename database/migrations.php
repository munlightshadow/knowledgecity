<?php

require "bootstrap.php";

use Illuminate\Database\Capsule\Manager as Capsule;

Capsule::schema()->create('api_users', function ($table) {
   $table->increments('id');
   $table->string('username')->unique();
   $table->string('password');
   $table->string('token')->nullable();
   $table->timestamps();
});

echo 'api_users tabel created' . PHP_EOL;

Capsule::schema()->create('students', function ($table) {
   $table->increments('id');
   $table->string('username');
   $table->string('name');
   $table->string('surname');
   $table->unsignedInteger('user_id');
   $table->foreign('user_id')->references('id')->on('api_users');
   $table->timestamps();
});

echo 'students tabel created' . PHP_EOL;